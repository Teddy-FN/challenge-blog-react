import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './pages/Home/Home';
import About from './pages/About/About';
import Contact from './pages/Contact/Contact';
import './App.css';
import { Switch, Route, Link } from 'react-router-dom';
import { Nav, NavItem } from 'reactstrap';
import './style.css'


function App() {
  return (
    <div>
      <div>
        <Nav tabs>
          <NavItem>
            <h2>NavBar</h2>
          </NavItem>
          <NavItem>
            <Link to='/' className="list">Home</Link>
          </NavItem>
          <NavItem>
            <Link to='/about' className="list">About</Link>
          </NavItem>
          <NavItem>
            <Link to='/contact' className="list">Contact</Link>
          </NavItem>
          <NavItem>
            <Link to='/topics' className="list">Topics</Link>
          </NavItem>
        </Nav>
      </div>

      <Switch>
        <Route path='/about'>
          <About />
        </Route>
        <Route path='/contact'>
          <Contact />
        </Route>
        <Route path='/'>
          <Home />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
