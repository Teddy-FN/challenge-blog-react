import React, { useState } from 'react';
import 'react-slideshow-image/dist/styles.css'
import {
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    CarouselCaption,
    Media,
    Card, Button, CardTitle, CardText, Row, Col,
} from 'reactstrap';
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";



const items = [
    {
        id: 1,
        altText: 'Slide 1',
        caption: 'Slide 1',
        image: './assets/music.jpg'
    },
    {
        id: 2,
        altText: 'Slide 2',
        caption: 'Slide 2',
        image: './assets/image2.jpg'
    },
    {
        id: 3,
        altText: 'Slide 3',
        caption: 'Slide 3',
        image: './assets/image3.jpg'
    }
];

const Home = (props) => {
    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);

    const next = () => {
        if (animating) return;
        const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
        setActiveIndex(nextIndex);
    }

    const previous = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
        setActiveIndex(nextIndex);
    }

    const goToIndex = (newIndex) => {
        if (animating) return;
        setActiveIndex(newIndex);
    }

    const slides = items.map((item) => {
        return (
            <CarouselItem
                className="custom-tag"
                tag="div"
                key={item.id}
                onExiting={() => setAnimating(true)}
                onExited={() => setAnimating(false)}
            >
                <CarouselCaption className="text-danger" captionText={item.caption} captionHeader={item.caption} />
            </CarouselItem>
        );
    });

    return (
        <div>
            <style>
                {
                    `.custom-tag {
              max-width: 100%;
              height: 500px;
              background: black;
            }`
                }
            </style>
            <Carousel
                activeIndex={activeIndex}
                next={next}
                previous={previous}>
                <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex}
                />
                {slides}
                <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />

                <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
            </Carousel>
            <aside >
                <div className='left'>
                    <div className="headline">
                        <Media className="mt-1">
                            <Media left middle href="#">
                                <Media object data-src="holder.js/64x64" alt="Generic placeholder image" />
                            </Media>
                            <Media body>
                                <Media heading>
                                    <h3>Media 1</h3>
                                </Media>
          Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
        </Media>
                        </Media>
                    </div>
                    <div className="headline">
                        <Media className="mt-1">
                            <Media left middle href="#">
                                <Media object data-src="holder.js/64x64" alt="Generic placeholder image" />
                            </Media>
                            <Media body>
                                <Media heading>
                                    <h3>Media 2</h3>
                                </Media>
          Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
        </Media>
                        </Media>
                    </div>
                    <div className="headline">
                        <Media className="mt-1">
                            <Media left middle href="#">
                                <Media object data-src="holder.js/64x64" alt="Generic placeholder image" />
                            </Media>
                            <Media body>
                                <Media heading>
                                    <h3>Media 3</h3>
                                </Media>
          Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
        </Media>
                        </Media>
                    </div>
                    <div className="headline">
                        <Media className="mt-1">
                            <Media left bottom href="#">
                                <Media object data-src="holder.js/64x64" alt="Generic placeholder image" />
                            </Media>
                            <Media body>
                                <Media heading>
                                    <h3>Media 4</h3>
                                </Media>
          Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
        </Media>
                        </Media>
                    </div>
                </div>
            </aside>
            <aside className="right-side">
                <div className='right'>
                    <Row>
                        <Col sm="4">
                            <Card body>
                                <CardTitle>Special Title Treatment</CardTitle>
                                <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                                <Button>Go somewhere</Button>
                            </Card>
                        </Col>
                        <Col sm="4">
                            <Card body>
                                <CardTitle>Special Title Treatment</CardTitle>
                                <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                                <Button>Go somewhere</Button>
                            </Card>
                        </Col>
                        <Col sm="4">
                            <Card body>
                                <CardTitle>Special Title Treatment</CardTitle>
                                <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                                <Button>Go somewhere</Button>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </aside>
            <footer>
                <div>
                    <MDBFooter color="blue" className="font-small pt-4 mt-4">
                        <MDBContainer fluid className="text-center text-md-left">
                            <MDBRow>
                                <MDBCol md="6">
                                    <h5 className="title">Footer Content</h5>
                                    <p>Here you can use rows and columns here to organize your footer
                                    content.</p>
                                </MDBCol>
                                <MDBCol md="6">
                                    <h5 className="title">Links</h5>
                                    <ul>
                                        <li className="list-unstyled">
                                            <a href="#!">Link 1</a>
                                        </li>
                                        <li className="list-unstyled">
                                            <a href="#!">Link 2</a>
                                        </li>
                                        <li className="list-unstyled">
                                            <a href="#!">Link 3</a>
                                        </li>
                                        <li className="list-unstyled">
                                            <a href="#!">Link 4</a>
                                        </li>
                                    </ul>
                                </MDBCol>
                            </MDBRow>
                        </MDBContainer>
                        <div className="footer-copyright text-center py-3">
                            <MDBContainer fluid>
                                &copy; {new Date().getFullYear()} Copyright: Teddy Ferdian
                            </MDBContainer>
                        </div>
                    </MDBFooter>
                </div>
            </footer>
        </div >
    );
}

export default Home;