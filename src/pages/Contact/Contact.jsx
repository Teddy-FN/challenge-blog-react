import React from 'react';
import '../../style.css'
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';

const Contact = () => {
    return (
        <div>
            <h1>Contact</h1>
            <div className="header">
                <Container className='page'>
                    <Form>
                        <FormGroup>
                            <Label for="exampleEmail"> Your Email : </Label>
                            <Input type="text" name="email" id="exampleEmail" placeholder="Write Your Email" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="name">Your Name : </Label>
                            <Input type="text" name="name" id="name" placeholder="Write Your Name" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="subject">Subject : </Label>
                            <Input type="text" name="subject" id="subject" placeholder="Write Your Subject" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="message">Your Message : </Label>
                            <Input type="textarea" name="message" id="message" placeholder="Your Message" />
                        </FormGroup>
                        <Button>Submit</Button>
                    </Form>
                </Container>
            </div>
        </div>
    )
}

export default Contact;