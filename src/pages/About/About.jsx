import React from 'react';
import { Card, Button, CardTitle, CardText, Row, Col, Container } from 'reactstrap';


const About = () => {
    return (
        <div>
            <h1>About Me</h1>
            <Container>
                <Row>
                    <Col sm="6">
                        <Card body>
                            <CardTitle tag="h5">Special Title Treatment</CardTitle>
                            <CardText> My name is Teddy Ferdian Abrar Amrullah you can call me
                            Teddy or Ferdi im graduated from Academy Technology Warga based on Surakarta
                            majoring in mechanical engineering im really like coding because im really love
                            to design and in the future i want to be a profesional programmer this is my
                            dream job.</CardText>
                            <Button>Go somewhere</Button>
                        </Card>
                    </Col>
                    <Col sm="6">
                        <Card body>
                            <CardTitle tag="h5">Special Title Treatment</CardTitle>
                            <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                            <Button>Go somewhere</Button>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default About;